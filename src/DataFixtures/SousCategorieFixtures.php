<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\Entity\SousCategorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SousCategorieFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // $product = new Product();
        // $manager->persist($product);
        $souscategorie = new SousCategorie();
        $souscategorie->setLibelle("SousCategorie1");
        //appel de la reference
        $souscategorie->setCategorie($this->getReference(CategorieFixtures::CATEGORIE_REFERENCE));
        $manager->persist($souscategorie);
        $manager->persist($souscategorie);

        $manager->flush();
    }
    //ajout de la fonction getDependencies
    public function getDependencies()
    {
        return array(
            CategorieFixtures::class,
        );
    }
}
