<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategorieFixtures extends Fixture
{
  public const CATEGORIE_REFERENCE = "categorie_id";

      public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $categorie = new Categorie();
        $categorie->setLibelle("Categorie1");

         $categorie->getSousCategories('SousCategorie');
            $this->setReference(self::CATEGORIE_REFERENCE, $categorie);

        $manager->persist($categorie);

        $manager->flush();
    }
}
