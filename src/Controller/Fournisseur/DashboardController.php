<?php

namespace App\Controller\Fournisseur;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/fournisseur")
     */
    public function index(): Response
    {
        #[Route('/', name: 'fournisseur')]
        return $this->render('fournisseur/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
