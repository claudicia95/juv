<?php

namespace App\Controller\Client;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractController
{
    /**
     * @Route("/client/dashboard")
     */
    public function index(): Response
    {
        #[Route('/', name: 'client_dashboard')]
        return $this->render('client/dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
